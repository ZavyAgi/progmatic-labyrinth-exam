package com.progmatic.labyrinthproject;

import com.progmatic.labyrinthproject.enums.CellType;
import com.progmatic.labyrinthproject.enums.Direction;
import com.progmatic.labyrinthproject.exceptions.CellException;
import com.progmatic.labyrinthproject.exceptions.InvalidMoveException;
import com.progmatic.labyrinthproject.interfaces.Labyrinth;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pappgergely
 */
public class LabyrinthImpl implements Labyrinth {

    private int height;
    private int width;
    CellType[][] labirynth;
    Coordinate playerPosition;

    public LabyrinthImpl() {
        height = 0;
        width = 0;
        playerPosition = new Coordinate(width, height);
    }

    @Override
    public void loadLabyrinthFile(String fileName) {
        try {
            Scanner sc = new Scanner(new File(fileName));
            int width = Integer.parseInt(sc.nextLine());
            int height = Integer.parseInt(sc.nextLine());
            setSize(width, height);
            labirynth = new CellType[width][height];
            CellType tip = null;
            for (int hh = 0; hh < height; hh++) {
                String line = sc.nextLine();
                for (int ww = 0; ww < width; ww++) {
                    switch (line.charAt(ww)) {
                        case 'W':
                            tip = CellType.WALL;
                            break;
                        case 'E':
                            tip = CellType.END;
                            break;
                        case 'S':
                            tip = CellType.START;
                            break;
                        case ' ':
                            tip = CellType.EMPTY;
                    }

                    Coordinate c = new Coordinate(ww, hh);
                    setCellType(c, tip);
                }
            }
        } catch (FileNotFoundException | NumberFormatException ex) {
            System.out.println(ex.toString());
        } catch (CellException ex) {
            Logger.getLogger(LabyrinthImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getWidth() {
        if (width <= 0) {
            return -1;
        } else {
            return width;
        }
    }

    @Override
    public int getHeight() {
        if (height <= 0) {
            return -1;
        } else {
            return height;
        }
    }

    @Override
    public CellType getCellType(Coordinate c) throws CellException {

        if (c.getRow() < 0 || c.getRow() >= width || c.getCol() < 0 || c.getCol() >= height) {
            String message = "Nincs ilyen terület";
            CellException ce = new CellException(c, message);
            throw ce;
        } else {
            return labirynth[c.getRow()][c.getCol()];
        }
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
        labirynth = new CellType[this.width][this.height];
    }

    @Override
    public void setCellType(Coordinate c, CellType type) throws CellException {

        if (c.getRow() < 0 || c.getRow() >= width || c.getCol() < 0 || c.getCol() >= height) {
            String message = "Nincs ilyen terület";
            CellException ce = new CellException(c, message);
            throw ce;
        } else {
            labirynth[c.getRow()][c.getCol()] = type;
        }
    }

    @Override
    public Coordinate getPlayerPosition() {
             return playerPosition;
        
    }
        @Override
        public boolean hasPlayerFinished() {
        try {
                CellType type = null;

                type = getCellType(playerPosition);

                if (type.equals(CellType.END)) {
                    return true;
                }

            } catch (CellException ex) {
                Logger.getLogger(LabyrinthImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return false;
        }

        @Override
        public List<Direction> possibleMoves
        
            () {
        List<Direction> directions = new ArrayList<>();
            int x = getPlayerPosition().getRow();
            int y = getPlayerPosition().getCol();
            if (labirynth[x - 1][y].equals(CellType.EMPTY)) {
                directions.add(Direction.WEST);
            } else if (labirynth[x + 1][y].equals(CellType.EMPTY)) {
                directions.add(Direction.EAST);
            } else if (labirynth[x][y - 1].equals(CellType.EMPTY)) {
                directions.add(Direction.SOUTH);
            } else if (labirynth[x][y + 1].equals(CellType.EMPTY)) {
                directions.add(Direction.NORTH);
            }
            return directions;
        }

        @Override
        public void movePlayer
        (Direction direction) throws InvalidMoveException {
            int oszlop = playerPosition.getCol();
            int sor = playerPosition.getRow();

        }

    }
